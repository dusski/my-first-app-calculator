﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        string input = string.Empty; //stores user input
        string operand1 = string.Empty; //stores first operand
        string operand2 = string.Empty; // stores second operand
        char operation; //char for operation
        double result = 0.0; //calculated result

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Display.Text = "";
            input += "1";
            this.Display.Text += input;
        }

        private void Display_TextChanged(object sender, EventArgs e)
        {

        }

        private void zero_Click(object sender, EventArgs e)
        {
            this.Display.Text = "";
            input += "0";
            this.Display.Text += input;
        }

        private void two_Click(object sender, EventArgs e)
        {
            this.Display.Text = "";
            input += "2";
            this.Display.Text += input;
        }

        private void three_Click(object sender, EventArgs e)
        {
            this.Display.Text = "";
            input += "3";
            this.Display.Text += input;
        }

        private void four_Click(object sender, EventArgs e)
        {
            this.Display.Text = "";
            input += "4";
            this.Display.Text += input;
        }

        private void five_Click(object sender, EventArgs e)
        {
            this.Display.Text = "";
            input += "5";
            this.Display.Text += input;
        }

        private void six_Click(object sender, EventArgs e)
        {
            this.Display.Text = "";
            input += "6";
            this.Display.Text += input;
        }

        private void seven_Click(object sender, EventArgs e)
        {
            this.Display.Text = "";
            input += "7";
            this.Display.Text += input;
        }

        private void eight_Click(object sender, EventArgs e)
        {
            this.Display.Text = "";
            input += "8";
            this.Display.Text += input;
        }

        private void nine_Click(object sender, EventArgs e)
        {
            this.Display.Text = "";
            input += "9";
            this.Display.Text += input;
        }

        private void dot_Click(object sender, EventArgs e)
        {
            this.Display.Text = "";
            input += ".";
            this.Display.Text += input;
        }

        private void erase_Click(object sender, EventArgs e)
        {
            this.Display.Text = "";
            this.input = string.Empty;
            this.operand1 = string.Empty;
            this.operand2 = string.Empty;
        }

        private void plus_Click(object sender, EventArgs e)
        {
            operand1 = input;
            operation = '+';
            input = string.Empty;
        }

        private void minus_Click(object sender, EventArgs e)
        {
            operand1 = input;
            operation = '-';
            input = string.Empty;
        }

        private void times_Click(object sender, EventArgs e)
        {
            operand1 = input;
            operation = '*';
            input = string.Empty;
        }

        private void divide_Click(object sender, EventArgs e)
        {
            operand1 = input;
            operation = '/';
            input = string.Empty;
        }

        private void equals_Click(object sender, EventArgs e)
        {
            operand2 = input;
            double num1, num2;
            double.TryParse(operand1, out num1);
            double.TryParse(operand2, out num2);

            if (operation == '+')
            {
                result = num1 + num2;
                Display.Text = result.ToString();
            }
            else if (operation == '-')
            {
                result = num1 - num2;
                Display.Text = result.ToString();
            }
            else if (operation == '*')
            {
                result = num1 * num2;
                Display.Text = result.ToString();
            }
            else if (operation == '/')
            {
                if (num2 != 0)
                {
                    result = num1 / num2;
                    Display.Text = result.ToString();
                }
                else
                {
                    Display.Text = "Can't divide by zero!";
                }
            }
        }
    }
}
